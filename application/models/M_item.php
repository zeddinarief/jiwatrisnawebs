<?php
class M_item extends CI_Model{	
	
	function tampilhome($table){
		$this->db->order_by('id_item', 'DESC');
		//$this->db->limit(3);
		$query = $this->db->get($table);
		return $query->result_array();
	}

	function tampilcap($where){
		$query = $this->db->get_where('item', $where);
		return $query->result_array();
	}

	function tampiltulis($where){
		$query = $this->db->get_where('item', $where);
		return $query->result_array();
	}

	function showcart($table,$where){
		$query = $this->db->get_where($table, $where);
		return $query->result_array();
	}

	// function tampilobwis($table){
	// 	$this->db->order_by('id_wisata', 'DESC');
	// 	$query = $this->db->get($table);
	// 	return $query->result_array();
	// }

	function pesan($table,$data){
		$this->db->insert($table , $data);
	}

	function batal($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
		
		
	}

}