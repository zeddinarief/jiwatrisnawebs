<?php
class M_user extends CI_Model{	
     function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
     }

     function selectmember($table,$where){		
		return $this->db->get_where($table,$where);
	}
     
	function is_user_available($where) {  
        
          $query = $this->db->get_where("users",$where);  
          if($query->num_rows() > 0) {  
               return true;  
          }  
          else {  
               return false;  
          }  
     }

     function insertMember($data,$table){
          $this->db->insert($table , $data);
     }

     function lihattransaksi(){
          $query = $this->db->query("SELECT * FROM transaksi JOIN item ON transaksi.id_item=item.id_item");
          
          return $query->result_array();
     }

}