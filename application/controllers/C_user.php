<?php
	class C_user extends CI_Controller {
		function __construct(){
			parent::__construct();			
			$this->load->model('M_item');
			}
		
		function index(){
			$this->item['item'] = $this->M_item->tampilhome('item');
			
			if($this->session->userdata('status') != "logged"){
				//$this->load->view('index');
				$this->load->view('index',$this->item);
			}else{
				//$this->load->view('index_m');
				$this->load->view('index_m',$this->item);
			}
		}

		function batikcap(){
			$where = array(
				'kategori' => 'batikcap'
				);
			$this->item['item'] = $this->M_item->tampiltulis($where);
			if($this->session->userdata('status') != "logged"){
				//$this->load->view('index');
				$this->load->view('batikcap',$this->item);
			}else{
				//$this->load->view('index_m');
				$this->load->view('batikcap_m',$this->item);
			}
		}

		function batiktulis(){
			$where = array(
				'kategori' => 'batiktulis'
				);
			$this->item['item'] = $this->M_item->tampiltulis($where);
			if($this->session->userdata('status') != "logged"){
				//$this->load->view('index');
				$this->load->view('batiktulis',$this->item);
			}else{
				//$this->load->view('index_m');
				$this->load->view('batiktulis_m',$this->item);
			}
		}

		function aboutus(){
			if($this->session->userdata('status') != "logged"){
				$this->load->view('about_us');
			}else{
				$this->load->view('about_us_m');
			}
		}

		function logout(){
			$this->session->sess_destroy();
			redirect(base_url());
		}

		// function registrasi(){
		// 	$this->load->view('signup');	
		// } 
		
		function cek_ketersediaan_user(){
	     	$where = array(
				'username' => $_POST["username"],
				
				);
	            $this->load->model("M_user");  
	            
	            if($this->M_user->is_user_available($where)) {  
	                echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Username Sudah Terdaftar</label>';  
	            }
	            else {  
	                echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Username Tersedia</label>';  
	            }  
	 	}

	 	function aksi_regis(){
			  $nama = $this->input->POST('addnama');
			  $username = $this->input->POST('addusername');
			  $password = $this->input->POST('addpassword');
			  //$confirmpass = $this->input->POST('confirmpass');
			  $email = $this->input->POST('addemail');
			  $telp = $this->input->POST('addtelp');

			  $where = array(
				'username' => $username,
				'password' => $password
				);
			  
			  $data = array( 
			   'nama' => $nama,
			   'username' => $username,
			   'email' => $email,
			   'password' => $password,
			   'telp' => $telp
			   );

			//   if ($password != $confirmpass) {
			//   		echo '<script type="text/javascript">alert("password tidak cocok !!");</script>';
			//   		$this->load->view('v_register');
			//   	# code...
			//   }	
			//   else{
			  	$this->load->model("M_user");  
				$this->M_user->insertMember($data,'users');
				$a = $this->M_user->selectmember("users",$where)->result_array();
			  	foreach ($a as $d) {}
				$data_session = array(
					'id' => $d['id_member'],
			    	'user' => $username,
			    	'password' => $password,
					'namalengkap' => $nama,
					//'alamat' => $alamat,
					'telp' => $telp,
					'email' => $email,
					//'jeniskelamin' => $gender,
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url());
			//   }

		}

		function beli($id){
			$where = array (
				'id_item' => $id
				);
			$this->item['itembeli'] = $this->M_item->showcart('item',$where);
			if($this->session->userdata('status') != "logged"){
				$this->load->view('index',$this->item);
			}else{
				$this->load->view('addcart',$this->item);
			}			
		}


		function keranjang(){
			$this->load->model('M_user');
			$this->src['transaksi'] = $this->M_user->lihattransaksi();
			$this->load->view('transaksi', $this->src);
		}

		
		function pesan(){
			$id = $this->input->POST('id');
			$jumlah = $this->input->POST('jumlah');
			$harga = $this->input->POST('harga');
			if($this->session->userdata('status') != "logged"){
				echo '<script type="text/javascript">alert("silahkan login dahulu!");</script>';
					$this->load->view('index');
			}else{
				if($jumlah > 0){
					$tgl = date('Y-m-d');
					$where = array(
						'id_item' => $id
						);
					$id_member = $this->session->userdata("id");
					$a = $this->M_item->showcart("item",$where);
					
					foreach ($a as $d) {}
					
					$data = array(
						'id_item' => $id,
						'id_member' => $id_member,
						'tanggal' => $tgl,
						'jumlah' => $jumlah,
						'total' => ($d['harga']*$jumlah)
						);
					// $this->item['detailobwis'] = $this->M_item->tampildetailobwis('objekitem',$where);
					// if ($d['status'] != 'tersedia') {
					// 	echo '<script type="text/javascript">alert("Maaf , paket tidak tersedia!");</script>';
					// 	$this->load->view('v_paketitem_m',$this->item);
					// }else{
					$this->M_item->pesan('transaksi',$data);
					echo '<script type="text/javascript">alert("pesanan berhasil di tambah ke keranjang!");</script>';
					echo "<script>history.go(-1);</script>";
					//}
				}else{
					echo '<script type="text/javascript">alert("input pada kolom jumlah tidak sesuai!");</script>';
					echo "<script>history.go(-1);</script>";
				}
			}
		}

		function bayar(){
			echo '<script type="text/javascript">alert("Pesanan Berhasil di bayar! . Terima Kasih telah belanja di toko kami :).");</script>';
			echo "<script>history.go(-2);</script>";
		}

		function batalpesan($id){
			$where = array(
				'id_transaksi' => $id
				);
			$this->M_item->batal('transaksi',$where);
			echo "<script>history.go(-1);</script>";
		}

	}