<?php
	class C_login extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->model('M_user');

		}

		function ceklogin(){
			$username = $this->input->POST('username');
			$password = $this->input->POST('password');
			$where = array(
				'username' => $username,
				'password' => $password
				);
			$cek = $this->M_user->cek_login("users",$where)->num_rows();
			$a = $this->M_user->cek_login("users",$where)->result_array();
			
			if($cek > 0){
				foreach ($a as $d) {}
				$data_session = array(
					'id' => $d['id_member'],
					'user' => $username,
					'namalengkap' => $d['nama'],
					'password' => $d['password'],
					'email' => $d['email'],
					//'alamat' => $d['alamat'],
					'telp' => $d['telp'],
					//'jeniskelamin' => $d['jeniskelamin'],
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url());
			}else{
				echo '<script type="text/javascript">alert("username atau password salah!!");</script>';
				$this->load->view('index');	
			}
		}
		
	}