<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Jiwatrisna</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <span class="navbar-brand mb-0 h1">
                <img src="<?php echo base_url()?>assets/img/jiwatrisna.png" height="30" class="d-inline-block align-top" alt="">
                Jiwatrisna
            </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batiktulis">Batik Tulis</a>
                        <a class="dropdown-item" href="<?php echo base_url()?>c_user/batikcap">Batik Cap</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>C_user/aboutus">Tentang Kami</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $this->session->userdata("namalengkap");?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/keranjang">Keranjang</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/logout">Logout</a>
                    </div>
                </li>
                
            </ul>
        </div>
        </div>
    </nav>

    <div class="container" style="margin-bottom: 30px">
        <h3 style="text-align: center;margin-top: -5px;">Keranjang Anda</h3>
        <hr>
        <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tr>
            <th class="col-md-1">No</th>
            <th class="col-md-2">Judul</th>
            <th class="col-md-1">Jumlah</th>
            <th class="col-md-1">Harga</th>
            <th class="col-md-2"></th>
            </tr>
            <?php
                        $no = 1;
                        $harga = 0;
                        foreach ($transaksi as $data){  
                        
                    ?>
            <tr>
            <th class="col-md-1"><?php echo $no++ ?></th>
            <th class="col-md-2"><?php echo $data['nama_item']; ?></th>
            <th class="col-md-1"><?php echo $data['jumlah']; ?></th>
            <th class="col-md-1"><?php echo $data['harga']; ?></th>
            <th class="col-md-2">
            <!-- 
            <button type="submit" class="btn btn-danger" id="tbt" onclick="return confirm('Apakah anda yakin ingin membatalkan?').href='<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>'"><h9 style="color: black">Batal</h9></button>
                <input type="button" class="btn btn-primary" id="login" value="Login" onclick="return confirm('Apakah anda yakin ingin membatalkan?').href='<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>'" /> -->
            <!-- <a class="btn btn-large btn-primary" data-toggle="confirmation" data-title="yakin ingin batal?"
        data-href="<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>" >Confirmation</a> -->
            <input type="button" class="btn btn-large btn-danger" name="delete" onclick="batalkan(<?php echo $data['id_transaksi'];?>)" value="Batal">
            </th>
            </tr>  
            <?php
                
                $harga += $data['harga'] * $data['jumlah'];;
            }
            ?>
        
        </table>
        <td style="text-align: right" colspan="6">Total Tagihan: Rp </td>
        <td><b>
            <?php echo $harga ?>
            </b>   
        </td>
        <td>   
            <a href="<?php echo base_url();?>C_user/bayar" class="btn btn-success" colspan="6" >Bayar sekarang</a>
        </td>
        </div>
                
        </div>
        

    <!-- Optional JavaScript -->
    <script type="text/javascript">
        function batalkan(id){
            if(confirm("Yakin ingin batal ?")){
                window.location.href="<?php echo base_url('c_user/batalpesan'); ?>/"+id;
            }
        }
    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
