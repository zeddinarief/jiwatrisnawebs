<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Jiwatrisna</title>
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <span class="navbar-brand mb-0 h1">
                <img src="<?php echo base_url()?>assets/img/jiwatrisna.png" height="30" class="d-inline-block align-top" alt="">
                Jiwatrisna
            </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batiktulis">Batik Tulis</a>
                        <a class="dropdown-item" href="<?php echo base_url()?>c_user/batikcap">Batik Cap</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>C_user/aboutus">Tentang Kami</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->session->userdata("namalengkap");?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/keranjang">Keranjang</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/logout">Logout</a>
                    </div>
                </li>
                
            </ul>
        </div>
        </div>
    </nav>

    <!-- Modal Tambah Keranjang -->
    <div class="modal fade" id="belimodal" tabindex="-1" role="dialog" aria-labelledby="Beli" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Tambah ke Keranjang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form method="post" action="<?php echo base_url()?>C_user/pesan">
                    <div class="modal-body">
                        <!-- <input type="text" class="form-control" name="idbeli" id="id"> -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Jumlah</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" name="jumlah" id="inputJumlah" placeholder="jumlah barang..">
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div> <!--end modal beli -->

    <div class="container">
        <h3>Batik Cap</h3>
        <hr>
    </div>

    <div class="container">
    
        <div class="row">
        <?php
        $no = 0;
        foreach ($item as $data){
            $no++;
        ?>
            <div class="col-sm-4 col-12 mb-4">
                <div class="card">
                    <img class="card-img-top img-fluid rounded mx-auto d-block" src="<?php echo base_url()?>assets/img/<?php echo $data['foto'] ?>" alt="Card image cap" style="height: 300px;width: auto">
                    <div class="card-body">
                        <h5 class="card-title text-truncate"><?php echo $data['nama_item'] ?></h5>
                        <p class="card-text">Rp <?php echo $data['harga'] ?></p>
                        <div class="row m-auto">
                            <!-- <button type="button" class="btn btn-info">Tambah ke Keranjang</button> -->
                            <!-- <a class="btn btn-info" data-toggle="modal" data-target="#belimodal" href="">Tambah ke Keranjang</a> -->
                            <a class="btn btn-info" href="<?php echo base_url()?>C_user/beli/<?php echo $data['id_item'] ?>">Tambah ke Keranjang</a>
                            <!-- <a href="" class="m-auto">Baca lengkap...</a> -->
                        </div>
                    </div>
                </div>
            </div>
        <?php   
        }
        ?>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- <script>
        function beli() {
            var id_item = $(this).data('id');
            $(".modal-body #id").val( id_item );
            $('#belimodal').modal('show');
        }
    </script> -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>