<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Jiwatrisna</title>
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <span class="navbar-brand mb-0 h1">
                <img src="<?php echo base_url()?>assets/img/jiwatrisna.png" height="30" class="d-inline-block align-top" alt="">
                Jiwatrisna
            </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
            
            <!-- Modal signin -->
            <div class="modal fade" id="signinmodal" tabindex="-1" role="dialog" aria-labelledby="SignIn" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="SignIn">Sign In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <form method="post" action="<?php echo base_url()?>C_login/ceklogin">
                        <div class="modal-body">
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Username</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" name="username" id="inputUsername" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Password">
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
                </div>
            </div> <!-- end Modal signin -->

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batiktulis">Batik Tulis</a>
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batikcap">Batik Cap</a>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <!-- Button trigger modal signin -->
                    <a class="nav-link" data-toggle="modal" data-target="#signinmodal" href="">Sign In</a>
                </li>
                <li class="nav-item">
                    <!-- Button trigger modal signup -->
                    <a class="nav-link" data-toggle="modal" data-target="#signupmodal" href="">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tentang Kami</a>
                </li>
                
            </ul>
        </div>
        </div>
    </nav>

    <div class="container">
    <form method="post" action="<?php echo base_url()?>C_login/aksi_regis">            
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="addemail" id="inputnewEmail" placeholder="Email">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama Lengkap</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="addnama" id="inputnewNama" placeholder="Nama Lengkap">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputusername" class="col-sm-2 col-form-label">Username</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="username" id="inputusername" placeholder="Username">
            </div>
        </div>
        <span id="user_check" style="margin-left: 200px"></span>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" name="addpassword" id="inputnewPassword" placeholder="Password">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Telepon</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="addtelepon" id="inputTelepon" placeholder="No Telepon">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Sign Up</button>

    </form>
    </div>

    <!-- Optional JavaScript -->
    <script type="text/javascript">
        $(document).ready(function(){  
            $('#inputusername').change(function(){  
                var username = $('#inputusername').val();  
                if(username != '') {  
                    $.ajax({  
                        url:"<?php echo base_url('C_user/cek_ketersediaan_user'); ?>",  
                        method:"POST",
                        data:{username},  
                        success:function(data){ 
                            $('#user_check').html(data);  
                        }  
                    });  
                }
                
            });  
        });  
    </script>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>