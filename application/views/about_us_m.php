<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Jiwatrisna</title>
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <span class="navbar-brand mb-0 h1">
                <img src="<?php echo base_url()?>assets/img/jiwatrisna.png" height="30" class="d-inline-block align-top" alt="">
                Jiwatrisna
            </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batiktulis">Batik Tulis</a>
                        <a class="dropdown-item" href="<?php echo base_url()?>c_user/batikcap">Batik Cap</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>C_user/aboutus">Tentang Kami</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?php echo $this->session->userdata("namalengkap");?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/keranjang">Keranjang</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/logout">Logout</a>
                    </div>
                </li>
                
            </ul>
        </div>
        </div>
    </nav>

    <div class="container">
        <h1 class="mb-3 mb-sm-3">Tentang Kami</h1>
        <hr>
        <img src="<?php echo base_url()?>assets/img/jiwatrisnalogo.png" alt="logo jiwatrina">
        <h3>Jiwatrisna Nusantara</h3>
        <p>Jawatrisna Nusantara Galeri merupakan perusahaan yang berpusat di Malang, Indonesia, yang memproduksi berbagai busana yang bermotifkan ciri khas Indonesia yaitu batik khususnya batik malangan. Jawatrisna Nusantara Galeri akan berusaha untuk tetap membuat para generasi muda khususnya mau menggunakan batik. Yang nantinya diharapkan batik akan tetap ada, sehingga generasi muda saat ini atau generasi dimasa yang akan mendatang akan tetap mengenal  batik.</p>
        <br><br>
        <h5><b>Informasi Lanjut</b></h5>
        <p>E-mail         : jiwatrisnanusantara@yahoo.com</p>
        <p>Instagram      : @jiwatrisna_galeri</p>
        <p>Nomor telepon  : 081230976667</p>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>
