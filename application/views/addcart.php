<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Jiwatrisna</title>
  </head>
  <body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light mb-4">
        <div class="container">
            <span class="navbar-brand mb-0 h1">
                <img src="<?php echo base_url()?>assets/img/jiwatrisna.png" height="30" class="d-inline-block align-top" alt="">
                Jiwatrisna
            </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Kategori
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/batiktulis">Batik Tulis</a>
                        <a class="dropdown-item" href="<?php echo base_url()?>c_user/batikcap">Batik Cap</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url()?>C_user/aboutus">Tentang Kami</a>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->session->userdata("namalengkap");?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/keranjang">Keranjang</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url()?>C_user/logout">Logout</a>
                    </div>
                </li>
                
            </ul>
        </div>
        </div>
    </nav>

    <div class="container">
    <?php foreach ($itembeli as $i) {?>
        <form method="post" action="<?php echo base_url()?>C_user/pesan">
        
            <!-- <input type="text" class="form-control" name="idbeli" id="id"> -->
            <h3>Rincian Pesanan</h3>
            <p><?php echo $i['nama_item'] ?></p>
            <p>Harga : <?php echo $i['harga'] ?></p>
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $i['id_item'] ?>">
            <input type="hidden" class="form-control" name="harga" id="harga" value="<?php echo $i['harga'] ?>">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jumlah</label>
                <div class="col-sm-1">
                    <input type="number" class="form-control" name="jumlah" id="inputJumlah" placeholder="0">
                </div>
                <button type="submit" class="btn btn-primary">Tambah ke Keranjang</button>
            </div>
            <div class="form-group row">
                
            </div>
            
        </form>
        <?php } ?>
    </div>

    <!-- Optional JavaScript -->
    <!-- <script>
        function beli() {
            var id_item = $(this).data('id');
            $(".modal-body #id").val( id_item );
            $('#belimodal').modal('show');
        }
    </script> -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>